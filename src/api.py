from os import getenv

from fastapi import FastAPI, Header
import sqlalchemy
import databases

database_url = f"postgresql://{getenv('DB_USER')}:{getenv('DB_PASSWORD')}@{getenv('DB_HOST')}/{getenv('DB')}"
database = databases.Database(database_url)

metadata = sqlalchemy.MetaData()
group_data = sqlalchemy.Table(
    "workshop",
    metadata,
    sqlalchemy.Column("group_name", sqlalchemy.Text, primary_key=True, nullable=False),
    sqlalchemy.Column("answer", sqlalchemy.Integer, nullable=False),
    sqlalchemy.Column("answered", sqlalchemy.Boolean, default=False),    
)

engine = sqlalchemy.create_engine(database_url, connect_args={"check_same_thread": False})
# metadata.create_all(engine)

app = FastAPI()

@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

@app.get("/")
async def root():
    return {"message": "Hello, world!"}


@app.post("/answer")
async def answer(answer: int, group_name: str = Header(None)):
    group = await database.fetch_one("SELECT * FROM workshop WHERE group_name = :group_name;", values={"group_name": group_name})
    if group:
        if group["answer"] == answer:
            await database.execute("UPDATE workshop SET answered = true WHERE group_name = :group_name;", values={"group_name": group_name})
            return {"answer": "correct"}
        else:
            return {"answer": "wrong"}
    else:
        return {"error": "incorrect group_name"}


@app.get("/fetch")
async def fetch():
    results = await database.fetch_all("SELECT answered FROM workshop;")
    if all([d["answered"] for d in results]):
        with open("answers_data_hex_scrambled.txt") as file:
            data = file.read().strip()
        return {"answer": data}
    else:
        return {"answer": "Not all groups have unlocked their part yet..."}
