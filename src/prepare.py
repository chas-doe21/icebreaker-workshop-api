from os import getenv
import json
import time
from binascii import unhexlify

import psycopg2


if __name__ == "__main__":
    # Give the db time to get up and running...
    time.sleep(5)
    connstr =f"postgresql://{getenv('DB_USER')}:{getenv('DB_PASSWORD')}@{getenv('DB_HOST')}/{getenv('DB')}"
    conn = psycopg2.connect(connstr)
    cur = conn.cursor()
    cur.execute("""CREATE TABLE workshop (group_name TEXT PRIMARY KEY NOT NULL,
                                          answer INT NOT NULL,
                                          answered BOOL DEFAULT false);""")
    with open("hexdata.json") as file:
        data = json.load(file)
    for group in data:
        bin_data = [c for c in unhexlify(data[group])]
        answer = 0
        for c in bin_data:
            if c > (answer // 10):
                answer += c
            else:
                answer //= c
        cur.execute(f"INSERT INTO workshop (group_name, answer) VALUES ('{group}', {answer});")
    conn.commit()
    print("DB setup successfully")
    cur.close()
    conn.close()